resource "aws_vpc" "feb_23_vpc" {
  cidr_block = "10.40.0.0/16"

  tags = {
    Name = "feb-23-vpc"
  }
}

resource "aws_subnet" "public_subnet_1" {
  vpc_id     = aws_vpc.feb_23_vpc.id
  cidr_block = "10.40.1.0/24"

  tags = {
    Name = "feb-23-vpc-public-subnet-1"
  }
}

resource "aws_subnet" "private_subnet_1" {
  vpc_id     = aws_vpc.feb_23_vpc.id
  cidr_block = "10.40.10.0/24"

  tags = {
    Name = "feb-23-vpc-private-subnet-1"
  }
}
